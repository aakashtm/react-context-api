import React,{useContext} from 'react'
import {ProvideTheme} from "./context_provider";



export function Square(){
    const [theme]=useContext(ProvideTheme);
    return(
      <div className="square" style={{backgroundColor:theme}}>

      </div>
    )
  }

  export function Circle(){
    const [theme]=useContext(ProvideTheme);
    return(
      <div className="circle" style={{backgroundColor:theme}}>
  
      </div>
    )
  }
  
  export function ThemePicker(){
    const [,setTheme]=useContext(ProvideTheme);  
  return(
      <div>
        <button style={{backgroundColor:"green"}} onClick={()=>{setTheme("green")}}>green</button>
        <button style={{backgroundColor:"red"}} onClick={()=>{setTheme("red")}}>red</button>
        <button style={{backgroundColor:"blue"}} onClick={()=>{setTheme("blue")}}>blue</button>
        <button style={{backgroundColor:"orange"}} onClick={()=>{setTheme("orange")}}>orange</button>
        <button style={{backgroundColor:"yellow"}} onClick={()=>{setTheme("yellow")}}>yellow</button>  
      </div>
     )  
  }